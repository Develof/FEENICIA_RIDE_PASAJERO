package example.com.sertitaxi;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.devmarvel.creditcardentry.library.CreditCardForm;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.card.payment.CardIOActivity;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class SignInWallet extends AppCompatActivity {

    private CreditCardForm numeroTarjeta;
    ImageView imgScanner, imgInfo;
    TextView txtExpiracion;
    Button btnAceptar;
    private int MY_SCAN_REQUEST_CODE = 10;
    DatePickerDialog dpd1;
    String cs, expDate;
    Boolean isExpDate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_wallet);

        numeroTarjeta = (CreditCardForm) findViewById(R.id.ed_wallet_numeroTarjeta);
        imgScanner = (ImageView) findViewById(R.id.btnCamaraOCR);
        imgInfo = (ImageView) findViewById(R.id.img_info_wallet);
        txtExpiracion = (TextView) findViewById(R.id.ed_wallet_expiracion);
        btnAceptar = (Button) findViewById(R.id.btn_aceptar);

        imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SimpleTooltip.Builder(SignInWallet.this)
                        .anchorView(v)
                        .contentView(R.layout.dialog_cvv_info, R.id.txt_cvv)
                        .gravity(Gravity.TOP)
                        .animated(true)
                        .showArrow(true)
                        .transparentOverlay(false)
                        .build()
                        .show();

            }
        });


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrado = new Intent(SignInWallet.this, RegistroActivado.class);
                startActivity(registrado);
            }
        });

        //OBTENEMOS EL MES Y AÑO ACTUAL PARA PONER LOS DATOS EN EL CAMPO DE FECHA DE EXPIRACIÓN (HINT DE EXPIRACIÓN)
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String hintFecha = sdf.format(cal.getTime());
        txtExpiracion.setHint(hintFecha);

        //AL DAR CLICK EN EL TEXTVIEW PARA PONER LA FECHA DE VENCIMIENTO DE LA TARJETA, ABRIREMOS UN DIALOG DATE PICKER COSTUMISADO
        txtExpiracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dpd1 = createDialogWithoutDateField();
                dpd1.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_signin_wallet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_omitir:
                Intent registrado = new Intent(SignInWallet.this, RegistroActivado.class);
                startActivity(registrado);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //DATE PICKER COSTUMIZADO
    private DatePickerDialog createDialogWithoutDateField() {

        final TextView firstDate = (TextView) findViewById(R.id.ed_wallet_expiracion);
        Date date = new Date();

        final Calendar c = GregorianCalendar.getInstance();
        c.setTime(date);

        MyDatePickerDialog dpd = new MyDatePickerDialog(this, android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
                SimpleDateFormat sdfExpDate = new SimpleDateFormat("yyMM");
                final Calendar calendar = new GregorianCalendar(year, (monthOfYear), dayOfMonth);
                cs = sdf.format(calendar.getTime());
                expDate = sdfExpDate.format(calendar.getTime());

                if (calendar.get(Calendar.YEAR) <= c.get(Calendar.YEAR)) {
                    if (calendar.get(Calendar.MONTH) < c.get(Calendar.MONTH)) {
                        isExpDate = false;
                        firstDate.setTextColor(Color.RED);
                    } else {
                        isExpDate = true;
                        firstDate.setTextColor(Color.argb(255, 255, 255, 255));
                    }
                    if (calendar.get(Calendar.YEAR) < c.get(Calendar.YEAR)) {
                        isExpDate = false;
                        firstDate.setTextColor(Color.RED);
                    }
                } else {
                    isExpDate = true;
                    firstDate.setTextColor(Color.argb(255, 255, 255, 255));
                }

                firstDate.setText(cs);

            }


        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        dpd.getDatePicker().init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // Notify the user.

            }
        });

        DatePicker a = dpd.getDatePicker();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.YEAR, 8);
        Date newDate = cal.getTime();

        dpd.invisibleDay(a);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000 + (1000 * 60 * 60 * 24 * 1));
        dpd.getDatePicker().setMaxDate(newDate.getTime());
        dpd.setPermanentTitle("Fecha de expiración");

        return dpd;

    }

    //METODO QUE EXTIENDE DE DATE PIKCER DIALOG PARA QUITAR EL DIA Y PONER LOS MESES CON NUMERO Y NO CON NOMBRE
    public class MyDatePickerDialog extends DatePickerDialog {

        private CharSequence title;

        public MyDatePickerDialog(Context context, int theme, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
            super(context, theme, callBack, year, monthOfYear, dayOfMonth);
        }

        public void setPermanentTitle(CharSequence title) {
            this.title = title;
            setTitle(title);
        }

        @Override
        public void onDateChanged(DatePicker view, int year, int month, int day) {
            super.onDateChanged(view, year, month, day);
            setTitle(title);
        }

        public void setCalendarViewShown(boolean shown) {

        }


        public void invisibleDay(final DatePicker dpDate) {
            try {
                String[] s = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
                    if (daySpinnerId != 0) {
                        View daySpinner = dpDate.findViewById(daySpinnerId);
                        if (daySpinner != null) {
                            daySpinner.setVisibility(View.GONE);
                        }
                    }

                    NumberPicker mMonthPicker = null;
                    int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
                    if (monthSpinnerId != 0) {
                        mMonthPicker = (NumberPicker) dpDate.findViewById(monthSpinnerId);
                        mMonthPicker.setDisplayedValues(s);
                    }

                    NumberPicker yearPicker = null;
                    int yearSpinnerId = Resources.getSystem().getIdentifier("year", "id", "android");
                    if (yearSpinnerId != 0) {
                        yearPicker = (NumberPicker) dpDate.findViewById(yearSpinnerId);
                    }

                    mMonthPicker.setOnValueChangedListener(new NumberPicker.
                            OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker picker, int
                                oldVal, int newVal) {
                            dpDate.updateDate(dpDate.getYear(), newVal, dpDate.getDayOfMonth());
                            String[] s = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
                            NumberPicker mMonthPicker = null;
                            int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
                            if (monthSpinnerId != 0) {
                                mMonthPicker = (NumberPicker) dpDate.findViewById(monthSpinnerId);
                                mMonthPicker.setDisplayedValues(s);
                            }
                        }
                    });

                    yearPicker.setOnValueChangedListener(new NumberPicker.
                            OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker picker, int
                                oldVal, int newVal) {
                            dpDate.updateDate(newVal, dpDate.getMonth(), dpDate.getDayOfMonth());
                            String[] s = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
                            NumberPicker mMonthPicker = null;
                            int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
                            if (monthSpinnerId != 0) {
                                mMonthPicker = (NumberPicker) dpDate.findViewById(monthSpinnerId);
                                mMonthPicker.setDisplayedValues(s);
                            }
                        }
                    });


                } else {

                    java.lang.reflect.Field[] Fields = DatePickerDialog.class.getDeclaredFields();
                    for (java.lang.reflect.Field datePickerDialogField : Fields) {
                        if (datePickerDialogField.getName().equals("mDatePicker")) {
                            datePickerDialogField.setAccessible(true);
                            DatePicker datePicker = null;
                            try {
                                datePicker = (DatePicker) datePickerDialogField.get(this);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();


                            Object dayPicker = new Object();
                            for (java.lang.reflect.Field datePickerField : datePickerFields) {
                                if ("mDaySpinner".equals(datePickerField.getName())) {
                                    datePickerField.setAccessible(true);
                                    try {
                                        dayPicker = datePickerField.get(datePicker);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                    ((View) dayPicker).setVisibility(View.GONE);

                                }

                                datePickerField.setAccessible(true);
                                if ("mMonthSpinner".equals(datePickerField.getName())) {
                                    datePickerField.setAccessible(true);
                                    NumberPicker monthPicker = null;
                                    try {
                                        monthPicker = (NumberPicker) datePickerField.get(datePicker);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                    monthPicker.setDisplayedValues(s);  //
                                }

                                if ("mShortMonths".equals(datePickerField.getName())) {
                                    datePickerField.setAccessible(true);
                                    try {
                                        datePickerField.set(datePicker, s);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    }
                }
            } catch (Exception e) {

            }
        }

    }


    //2 metodos para escanear y mostrar el num tarjeta onScanPress y onActivityResult
    public void onScanPress(View v) {
        Intent intent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false);
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false);
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false);
        intent.putExtra(CardIOActivity.EXTRA_RESTRICT_POSTAL_CODE_TO_NUMERIC_ONLY, false);
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false);

        /*quitamos tema icono de paypau*/

        // hides the manual entry button
        // if set, developers should provide their own manual entry mechanism in the app
        intent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

        // matches the theme of your application
        intent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true); // default: false

        //icono
        intent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(intent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String tarjeta = "";
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            io.card.payment.CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            //resultStr=scanResult.getRedactedCardNumber();  -->    regresa "**** **** **** 1234" no muestra todos los numeros de la tarjeta
            tarjeta = scanResult.cardNumber;    //   -->devuelve los 16 digitos de la tarjeta
        } else {
            numeroTarjeta.setCardNumber("", true);
        }
        numeroTarjeta.setCardNumber(tarjeta, true);
    }

}
