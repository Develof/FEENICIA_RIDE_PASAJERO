package example.com.sertitaxi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import example.com.sertitaxi.constantes.Constantes;
import example.com.sertitaxi.models.UsuarioDBModel;
import example.com.sertitaxi.sqlhelper.UsuarioBDHelper;

public class RegistroActivado extends AppCompatActivity {

    Button btnComenzarViaje;
    TextView txtUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registroactivado);

        UsuarioBDHelper usuarioBDHelper = new UsuarioBDHelper(RegistroActivado.this, null, Constantes.VERSION_BASE_DE_DATOS);
        UsuarioDBModel usuario = new UsuarioDBModel();

        usuario.setNombre("Omar");
        usuario.setCelular("05534364605");
        usuario.setCorreo("d");
        usuario.setIdUsuario(1);

        btnComenzarViaje = (Button) findViewById(R.id.btn_comenzar_viaje);
        txtUserName = (TextView) findViewById(R.id.txt_registro_activado_usuario);

        txtUserName.setText(usuario.getNombre());

        btnComenzarViaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buscarViaje = new Intent(RegistroActivado.this, BuscarViaje.class);
                startActivity(buscarViaje);
            }
        });

    }
}
