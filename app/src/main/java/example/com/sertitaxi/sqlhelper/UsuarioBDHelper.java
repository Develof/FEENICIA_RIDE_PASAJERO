package example.com.sertitaxi.sqlhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import example.com.sertitaxi.database.DBHelper;
import example.com.sertitaxi.models.UsuarioDBModel;

/**
 * Created by Develof on 16/01/17.
 */

public class UsuarioBDHelper extends DBHelper {


    public UsuarioBDHelper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, factory, version);
    }

    public void createUser(UsuarioDBModel usuario) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues userValues = new ContentValues();
        userValues.put(USUARIO_NOMBRE, "Omar");
        userValues.put(USUARIO_CORREO, "correo");
        userValues.put(USUARIO_CELULAR, "5534364606");
        database.insert(TABLA_USUARIO, null, userValues);

        database.close();
    }

    public UsuarioDBModel getUsuario() {
        UsuarioDBModel usuario = null;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select * from " + TABLA_USUARIO, null);
        if (cursor.moveToFirst()) {
            int idUsuario = Integer.parseInt(cursor.getString(0));
            String nombre = cursor.getString(1);
            String correo = cursor.getString(2);
            String celular = cursor.getString(3);

            usuario = new UsuarioDBModel(idUsuario, nombre, correo, celular);
        }

        cursor.close();
        db.close();

        return usuario;
    }

}
