package example.com.sertitaxi.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import example.com.sertitaxi.R;

/**
 * Created by Develof on 14/01/17.
 */

public class CustomDialogViaje extends DialogFragment implements TextView.OnEditorActionListener {

    private String distancia;
    private String duracion;
    private double valorDistancia;
    private double valorDuracion;
    private Handler mHandler = new Handler();
    ProgressDialog myDialog;
    Button btnPediTaxi;
    RelativeLayout rlConductor;
    String destino;
    boolean isTaxiComing = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.distancia = getArguments().getString("distancia");
        this.duracion = getArguments().getString("duracion");
        this.valorDistancia = getArguments().getDouble("vDistancai");
        this.valorDuracion = getArguments().getDouble("vDuracion");
        this.destino = getArguments().getString("destino");

    }

    public static CustomDialogViaje newInstance(String distancia, String duracion, double valorDistancia, double valorDuracion, String destino) {

        Bundle args = new Bundle();

        CustomDialogViaje fragment = new CustomDialogViaje();
        args.putString("distancia", distancia);
        args.putString("duracion", duracion);
        args.putDouble("vDistancai", valorDistancia);
        args.putDouble("vDuracion", valorDuracion);
        args.putString("destino", destino);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Window window = getDialog().getWindow();

        // set "origin" to top left corner, so to speak
        window.setGravity(Gravity.BOTTOM);

        // after that, setting values for x and y works "naturally"
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = -2;
        params.y = 30;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        getDialog().getWindow().setAttributes(params);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_start_ride, container, false);


        TextView txtDistancia = (TextView) v.findViewById(R.id.tv_start_ride_distancia);
        TextView txtDuracion = (TextView) v.findViewById(R.id.tv_start_ride_tiempo);
        TextView txtPrecio = (TextView) v.findViewById(R.id.tv_start_ride_costo);
        TextView txtDestino = (TextView) v.findViewById(R.id.tv_start_ride_destino);
        btnPediTaxi = (Button) v.findViewById(R.id.btn_pedir_taxi);
        rlConductor = (RelativeLayout) v.findViewById(R.id.rl_conductor);

        txtDestino.setText(destino);
        txtDistancia.setText(distancia);
        txtDuracion.setText(duracion);

        valorDistancia = valorDistancia / 1000;
        valorDuracion = valorDuracion / 60;

        txtPrecio.setText((valorDistancia * valorDuracion) / 10 + " $");

        btnPediTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isTaxiComing) {
                    mHandler.postDelayed(mUpdateTaxi, 10000);
                    myDialog = new ProgressDialog(getActivity());
                    myDialog.setMessage("Buscando Taxi...");
                    myDialog.setCancelable(false);
                    myDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mHandler.removeMessages(0);
                            dialog.dismiss();
                        }
                    });
                    myDialog.show();
                } else {
                    dismiss();
                }
            }
        });

        return v;
    }

    private Runnable mUpdateTaxi = new Runnable() {
        public void run() {
            isTaxiComing = true;
            btnPediTaxi.setText("Aceptar");
            rlConductor.setVisibility(View.VISIBLE);
            myDialog.dismiss();
        }
    };


    @Override
    public int getTheme() {
        return R.style.MyCustomTheme;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }
}