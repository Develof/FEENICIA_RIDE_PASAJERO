package example.com.sertitaxi.models;

/**
 * Created by Develof on 16/01/17.
 */

public class UsuarioDBModel {

    private int idUsuario;
    private String nombre;
    private String correo;
    private String celular;

    public UsuarioDBModel(){}

    public UsuarioDBModel(int idUsuario, String nombre, String correo, String celular) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.correo = correo;
        this.celular = celular;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
}
