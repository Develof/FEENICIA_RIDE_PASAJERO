package example.com.sertitaxi;

import android.*;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsSession;
import com.digits.sdk.android.SessionListener;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import example.com.sertitaxi.constantes.Constantes;
import example.com.sertitaxi.database.DBHelper;
import example.com.sertitaxi.models.UsuarioDBModel;
import example.com.sertitaxi.sqlhelper.UsuarioBDHelper;
import io.fabric.sdk.android.Fabric;

import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "KaIQf0AE3iOwHOWmY4K6Y3glE";
    private static final String TWITTER_SECRET = "yuCjACWOat9PcZEO7nCBNuT6JfMrOWE0FdNsOoL9z3BACEVyFo";

    // CERO ES EL TIPO DE USUARIO NO CONDUCTOR  /  1 ES TIPO DE USUARIO CONDUCTOR;
    VideoView video;
    SessionListener sessionListener;
    boolean isUserActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new Digits.Builder().build());
        setContentView(R.layout.activity_login);

        Button btn_entrar = (Button) findViewById(R.id.btn_login_entrar);
        video = (VideoView) findViewById(R.id.videoView);


        UsuarioBDHelper usuarioBDHelper = new UsuarioBDHelper(Login.this, null, Constantes.VERSION_BASE_DE_DATOS);
        UsuarioDBModel usuario = usuarioBDHelper.getUsuario();
        if (usuario != null) {
            btn_entrar.setText("VIAJAR");
            isUserActive = true;
        } else {
            btn_entrar.setText("CREAR CUENTA");
            isUserActive = false;
        }


       /* imgUsuario.setOnClickListener(v -> {
            imgUsuario.setBackground(ContextCompat.getDrawable(Login.this, R.color.colorSecondaryDark));
            imgConductor.setBackground(ContextCompat.getDrawable(Login.this, R.color.colorSecondary));
            userType = USUARIO;
        });

        imgConductor.setOnClickListener(v -> {
            imgUsuario.setBackground(ContextCompat.getDrawable(Login.this, R.color.colorSecondary));
            imgConductor.setBackground(ContextCompat.getDrawable(Login.this, R.color.colorSecondaryDark));
            userType = CONDUCTOR;
        });  */

        btn_entrar.setOnClickListener(v -> {

            askForContactPermission();

        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Digits.getActiveSession() != null) {
            Toast.makeText(Login.this, "YA", Toast.LENGTH_SHORT).show();
        }

        if (new Digits.Builder().build().getActiveSession() != null) {
            Toast.makeText(Login.this, "AHORA SI", Toast.LENGTH_SHORT).show();
        }

        sessionListener = new SessionListener() {
            @Override
            public void changed(DigitsSession newSession) {
                Toast.makeText(Login.this, "Session phone was changed: " + newSession
                        .getPhoneNumber(), Toast.LENGTH_SHORT).show();
            }
        };
        Digits.addSessionListener(sessionListener);


        video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash));
        video.start();

        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                video.resume();
                video.start(); //need to make transition seamless.
            }
        });
    }


    @Override
    protected void onStop() {
        Digits.removeSessionListener(sessionListener);
        super.onStop();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int ubicacionPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> listPermissionsNeeded = new ArrayList<>();

            if (ubicacionPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }


            if (!listPermissionsNeeded.isEmpty()) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
                return;
                //  requestPermissions(new String[]{android.Manifest.permission.BLUETOOTH_ADMIN}, 100);
            } else {
                // No explanation needed, we can request the permission.
                if (isUserActive) {
                    Intent viajar = new Intent(Login.this, BuscarViaje.class);
                    startActivity(viajar);
                } else {
                    Intent home = new Intent(Login.this, SignIn.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(home);
                }

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            if (isUserActive) {
                Intent viajar = new Intent(Login.this, BuscarViaje.class);
                startActivity(viajar);
            } else {
                Intent home = new Intent(Login.this, SignIn.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(home);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent ingresar = new Intent(Login.this, SignIn.class);
                    ingresar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(ingresar);
                } else {
                    // Permission Denied
                    Toast.makeText(Login.this, "Para poder usar la aplicación debes dar los permisos, podras agregarlos manualmente", Toast.LENGTH_SHORT).show();
                    if (isUserActive) {
                        Intent viajar = new Intent(Login.this, BuscarViaje.class);
                        startActivity(viajar);
                    } else {
                        Intent home = new Intent(Login.this, SignIn.class);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(home);
                    }
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
