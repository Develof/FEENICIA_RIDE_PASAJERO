package example.com.sertitaxi;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.AuthConfig;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;

import java.net.URISyntaxException;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import example.com.sertitaxi.constantes.Constantes;
import example.com.sertitaxi.models.UsuarioDBModel;
import example.com.sertitaxi.sqlhelper.UsuarioBDHelper;

public class SignIn extends AppCompatActivity {

    private Socket mSocket;


    {
        try {
            mSocket = IO.socket("http://54.203.245.137:1337");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    EditText edUsuario, edCorreo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mSocket.connect();
        mSocket.emit("connect");


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_title);

        edUsuario = (EditText) findViewById(R.id.etUserName);
        edCorreo = (EditText) findViewById(R.id.etCorreo);

        Digits.enableSandbox();

        TextView txtSiguiente = (TextView) findViewById(R.id.tv_registro_siguiente);
        txtSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edUsuario.getText().toString().isEmpty() && !edCorreo.getText().toString().isEmpty()) {

                    AuthConfig.Builder builder = new AuthConfig.Builder();

                    builder.withAuthCallBack(new AuthCallback() {
                        @Override
                        public void success(DigitsSession session, String phoneNumber) {

                            UsuarioDBModel usuario = new UsuarioDBModel(1, edUsuario.getText().toString(), edCorreo.getText().toString(), "5534364606");
                            UsuarioBDHelper usuarioDbHelper = new UsuarioBDHelper(SignIn.this, null, Constantes.VERSION_BASE_DE_DATOS);
                            usuarioDbHelper.createUser(usuario);

                            Intent singWallet = new Intent(SignIn.this, SignInWallet.class);
                            startActivity(singWallet);

                     /*       JSONObject jsonUser = new JSONObject();
                            try {
                                jsonUser.put("name", edUsuario.getText().toString());
                                jsonUser.put("phone", "5534364607");
                                jsonUser.put("lastName", "Flores");
                                jsonUser.put("lastSecondName", "Bolaños");
                                // json.put("")
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            JSONObject json = new JSONObject();
                            try {
                                json.put("method", "get");
                                json.put("url", "/user/emitirMensaje");
                                //  json.put("data", jsonUser);
                                // json.put("")
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mSocket.emit("post", json, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    String status = "";
                                    try {
                                        JSONObject obj = new JSONObject(args[0].toString());
                                        status = obj.getString("statusCode");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (status.equals("200") || status == "200") {
                                        UsuarioDBModel usuario = new UsuarioDBModel(1, edUsuario.getText().toString(), edCorreo.getText().toString(), "5534364606");
                                        UsuarioBDHelper usuarioDbHelper = new UsuarioBDHelper(SignIn.this, null, Constantes.VERSION_BASE_DE_DATOS);
                                        usuarioDbHelper.createUser(usuario);
                                        Intent singWallet = new Intent(SignIn.this, SignInWallet.class);
                                        startActivity(singWallet);
                                    }

                                }
                            });  */

                        }

                        @Override
                        public void failure(DigitsException error) {
                            // Do something
                            Log.v("DIGITS ERROR", error.getMessage());
                            Toast.makeText(SignIn.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                    AuthConfig authConfig = builder.build();

                    Digits.authenticate(authConfig);

                } else {
                    Toast.makeText(SignIn.this, "Llena todos los campos para continuar", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private Emitter.Listener onNewUser = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            int numUsers;
            try {
                numUsers = data.getInt("numUsers");
            } catch (JSONException e) {
                return;
            }

            Intent intent = new Intent();
            intent.putExtra("numUsers", numUsers);
            setResult(RESULT_OK, intent);
            finish();
        }

    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }


}
