package example.com.sertitaxi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import example.com.sertitaxi.constantes.Constantes;
import example.com.sertitaxi.models.UsuarioDBModel;
import example.com.sertitaxi.sqlhelper.UsuarioBDHelper;

public class BuscarViaje extends AppCompatActivity {

    Button btnBuscarViaje;
    int PLACE_PICKER_REQUEST = 1;
    TextView txtUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_viaje);

        UsuarioBDHelper usuarioBDHelper = new UsuarioBDHelper(BuscarViaje.this, null, Constantes.VERSION_BASE_DE_DATOS);
        UsuarioDBModel usuario = usuarioBDHelper.getUsuario();
        txtUsuario = (TextView) findViewById(R.id.txt_registro_activado_usuario);

        btnBuscarViaje = (Button) findViewById(R.id.ed_buscar_viaje);

        txtUsuario.setText("Hola " + usuario.getNombre());

        btnBuscarViaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    startActivityForResult(builder.build(BuscarViaje.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } */
                Intent destino = new Intent(BuscarViaje.this, UbicarDestino.class);
                startActivity(destino);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
              //  String toastMsg = String.format("Lugar es: ", place.getName());
                String toastMsg =  place.getName().toString();
                btnBuscarViaje.setText(toastMsg);
            }
        }
    }
}
