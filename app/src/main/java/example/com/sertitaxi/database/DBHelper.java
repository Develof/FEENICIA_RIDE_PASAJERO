package example.com.sertitaxi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Develof on 16/01/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "FeeniciaRide.db";

    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
    }

    public static final String TABLA_USUARIO = "Usuario";
    public static final String ID_USUARIO = "ID_USUARIO";
    public static final String USUARIO_NOMBRE = "Nombre";
    public static final String USUARIO_CORREO = "Correo";
    public static final String USUARIO_CELULAR = "Celular";

    private static final String USUARIO = "create table " + TABLA_USUARIO +
            "(" + ID_USUARIO + " integer primary key, " + USUARIO_NOMBRE + " text, " + USUARIO_CORREO + " text, " + USUARIO_CELULAR + " text)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_USUARIO);
        onCreate(db);
    }
}
